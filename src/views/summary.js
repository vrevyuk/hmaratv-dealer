import React from 'react'
import { connect } from 'react-redux'
import {bindActionCreators} from "redux";

import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";

import getConfig from '../actions/get-config'

class Summary extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	componentDidMount() {
		let { token, language } = this.props;
		this.props.getConfig({ token, language });
	}

	render() {

		let { dictionary = {}, language } = this.props.state;

		const availableKeys = ['name','balance','profit','discount','referralUrl'];

		return (
			<Table>
				<TableBody>
					{Object.keys(this.props.state).filter(key => availableKeys.includes(key)).map( (property, key) => <TableRow key={key}>
						<TableCell>{dictionary[property][language]}</TableCell>
						<TableCell>{this.props.state[property]}</TableCell>
					</TableRow>)}
				</TableBody>
			</Table>
		)
	}
}

function mapStateToProps(state) {
	let { error, time, busy, dealerId, name, balance, discount, profit, credits, consumers,
		messages, packets, token, dictionary, language, referralUrl } = state;
	return {
		state: {
			error, time, busy, dealerId, name, balance, profit, discount, credits,
			consumers, messages, packets, token, dictionary, language, referralUrl,
		}
	}
}

function matDispatchToProps(dispatch) {
	return bindActionCreators({
		getConfig
	}, dispatch);
}

export default connect(mapStateToProps, matDispatchToProps)(Summary);