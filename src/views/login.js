/**
 * Created by Vitaly Revyuk on 10/31/18.
 */
import React from 'react'
import { connect } from 'react-redux'

import { Panel, Grid, Row, Col, Form, FormGroup, InputGroup, FormControl, Button, Checkbox, Glyphicon } from 'react-bootstrap'
import DismissableAlert from './alert-with-dismiss'

import { removeSession, requestSMS, checkCode } from '../api'

class LoginView extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			busy: false,
			refreshToken: false,
			saveSession: false,
			smsSent: false,
			smsConfirmed: false,

			language: 'en',
			phone: '',
			code: '',
		}
	}

	componentDidMount() {
		let { refreshToken, language } = this.props;
		this.setState({ saveSession: !!refreshToken, language });
	}

	requestCODE() {
		let { phone } = this.state;
		if (phone.length < 5) return;
		this.props.requestSMS({
			phone, cb: () => this.setState({
				smsSent: true
			})
		} );
	}

	checkCODE() {
		let { phone, code, language, saveSession } = this.state;
		this.props.checkCode({ language, phone, code, saveSession });
	}

	requestToken() {
		let { refreshToken } = this.props;
		let { language } = this.state;
		this.props.checkCode({ language, refreshToken, saveSession: true });
	}

	removeSession() {
		this.props.removeSession()
	}

	reset() {
		this.setState({
			phone: '',
			code: '',
			smsSent: false,
			smsConfirmed: false,
		});
	}

	onKeyPress(e) {
		e.preventDefault();
		console.log('ON KEY PRESS', e.target);
	}

	onChange(e) {

		let { id, value, checked } = e.target;
		switch (id) {
			case 'language':
				this.setState({ language: value });
				break;
			case 'phone':
				this.setState({ phone: value });
				break;
			case 'code':
				this.setState({ code: value });
				break;
			case 'saveSession':
				this.setState({ saveSession: checked });
				break;
			default: break;
		}

	}

	render() {

		let { smsSent, smsConfirmed, saveSession, language, phone, code } = this.state;
		let { refreshToken, error, busy, dictionary = {} } = this.props;
		let { appTitle, phonePlaceholder, codePlaceholder, restoreSessionBtn, removeSessionBtn, saveSessionLabel } = dictionary;

		return <Grid fluid><Row>
			<Col>&nbsp;</Col>
		</Row><Row>
			<Col sm={1}  md={2} lg={3}/>
			<Col sm={10} md={8} lg={6}><Panel>
				<Panel.Heading>
					<b>{appTitle[language]}</b>
				</Panel.Heading>
				<Panel.Body>
					<Form>
						<FormGroup controlId="language"><InputGroup>
							<InputGroup.Addon><Glyphicon glyph="globe"/></InputGroup.Addon>
							<FormControl componentClass="select"
										 value={language} onChange={this.onChange.bind(this)}>
								<option value="en">English</option>
								<option value="uk">Українська</option>
								<option value="pl">Polski</option>
								<option value="ru">Русский</option>
							</FormControl>
						</InputGroup></FormGroup>

						<FormGroup controlId="phone"><InputGroup>
							<InputGroup.Addon><Glyphicon glyph="phone"/></InputGroup.Addon>
							<FormControl disabled={smsSent || busy}
										 type="tel" value={phone}
										 onChange={this.onChange.bind(this)}
										 placeholder={phonePlaceholder[language]}/>
							<InputGroup.Button>
								<Button disabled={smsSent || busy}
										onClick={this.requestCODE.bind(this)}>
									<Glyphicon glyph="send"/>
								</Button>
							</InputGroup.Button>
							{phone.length > 0 ? (
									<InputGroup.Button>
										<Button onClick={this.reset.bind(this)}><Glyphicon glyph="remove"/></Button>
									</InputGroup.Button>
								) : null}
						</InputGroup></FormGroup>
						<FormGroup controlId="saveSession">
							<Checkbox disabled={smsSent || busy} checked={saveSession} inline onChange={this.onChange.bind(this)} id="saveSession">{saveSessionLabel[language]}</Checkbox>
						</FormGroup>
						<FormGroup controlId="code"><InputGroup>
							<InputGroup.Addon><Glyphicon glyph="envelope"/></InputGroup.Addon>
							<FormControl type="tel" disabled={!smsSent || smsConfirmed || busy} value={code} onChange={this.onChange.bind(this)} placeholder={codePlaceholder[language]}/>
							<InputGroup.Button>
								<Button disabled={!smsSent || smsConfirmed || busy} onClick={this.checkCODE.bind(this)}><Glyphicon glyph="ok"/></Button>
							</InputGroup.Button>
						</InputGroup></FormGroup>
					</Form>
					{error ? <DismissableAlert bsStyle="danger">{error}</DismissableAlert> : null}
				</Panel.Body>{refreshToken ? (
						<Panel.Footer>
							<Button disabled={busy} onClick={this.requestToken.bind(this)}>{restoreSessionBtn[language]}</Button>
							{' '}
							<Button disabled={busy} onClick={this.removeSession.bind(this)}>{removeSessionBtn[language]}</Button>
						</Panel.Footer>
					) : null}
			</Panel></Col>
			<Col sm={1}  md={2} lg={3}/>
		</Row><Row>
			<Col>&nbsp;</Col>
		</Row></Grid>
	}
}

function mapStateToProps(state) {
	let { refreshToken, error, busy, sentSMS, captchaRequired, checkedSMS, id, name, language, dictionary } = state;
	return {
		refreshToken,
		error,
		busy,
		sentSMS,
		captchaRequired,
		checkedSMS,
		id,
		name,
		language,
		dictionary,
	}
}

function mapDispatchToProps(dispatch) {
	return {
		removeSession: () => removeSession(dispatch),
		requestSMS: (data) => requestSMS(data, dispatch),
		checkCode: (data) => checkCode(data, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginView);