/**
 * Created by Vitaly Revyuk on 10/31/18.
 */
import React from 'react'
import { connect } from 'react-redux'
import {bindActionCreators} from "redux";
import {Redirect} from 'react-router'
import moment from "moment";

import 'react-bootstrap-toggle/dist/bootstrap2-toggle.css';

import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";

import dismissAlert from '../actions/dismiss-alert'
import sendSMS2consumer from '../actions/send-sms-2-consumer'
import checkConsumerSMS from '../actions/check-consumer-sms'
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {TextField} from "@material-ui/core";

class JoinConsumer extends React.Component {
	constructor(props) {
		super(props);
		this.timerNextSMS = null;
		this.state = {
			contact: '',
			code: '',
			addedPackets: [],
			enableDiscount: true,
			enableTest: true,
			timerNextSMS: null,
			timerCounter: 0,
			enableSendButton: false,
			disableSendButton: true,
		}
	}

	componentDidMount() {
		this.props.dismissAlert();
	}

	componentWillUnmount() {
		clearTimeout(this.timerNextSMS);
	}

	onChangeInput(e) {
		let { id, value, checked, type } = e.target;
		this.setState({[id]: type === 'checkbox' ? checked : value});
	}

	timerUpdater() {
		let {remainSMS, nextSMS_attempt} = this.props.state;
		const enableSendButton = remainSMS > 0 && nextSMS_attempt <= moment().unix();
		this.setState({
			timerCounter:  remainSMS > 0 ? Math.abs(moment().unix() - nextSMS_attempt) : 0,
			disableSendButton: !enableSendButton,
		}, () => {
			if (!enableSendButton && remainSMS > 0) {
				this.timerNextSMS = setTimeout(() => this.timerUpdater(), 1000)
			}
		});
	}

	sendSMS() {
		let {contact} = this.state;
		clearTimeout(this.timerNextSMS);
		this.setState({
			timerCounter: 0,
		}, () => {
			this.props.sendSMS2consumer({contact, cb: () => this.timerUpdater()});
		});
	}

	checkSMS() {
		let { contact, code, addedPackets, enableDiscount, enableTest } = this.state;
		this.props.checkConsumerSMS({
			contact,
			code,
			packets: addedPackets.map( p => p.id).join(','),
			enableDiscount,
			enableTest,
		});

	}

	openConsumer() {
		let {newConsumerId} = this.props.state;
		this.props.history.push(`/consumer/${newConsumerId}`)
	}

	onTogglePackets(packet = {}) {
		let { addedPackets = [] } = this.state;

		if(addedPackets.includes(packet)) {
			this.setState({ addedPackets: addedPackets.filter(p => p.id !== packet.id) });
		}  else {
			this.setState({ addedPackets: addedPackets.filter(p => p.groupId !== packet.groupId || p.groupId === 0 ).concat(packet)});
		}
	}

	reset() {
		this.setState({
			contact: undefined,
			code: undefined,
			addedPackets: [],
			enableDiscount: 1,
			enableTest: 1,
		}, () => this.props.dismissAlert());
	}

	onKeyPress(e) {
		if (e.key === 'Enter') {
			let {consumerJoinPosition} = this.props.state;
			e.preventDefault();
			if (consumerJoinPosition === 0) {
				return this.sendSMS();
			} else if (consumerJoinPosition === 1) {
				return  this.checkSMS();
			}
		}
	}

	render() {

		let {contact, code, addedPackets, enableTest, enableDiscount, timerCounter, disableSendButton} = this.state;
		let {
			busy, language, dictionary = {}, packets,
			consumerJoinPosition: position, remainSMS, nextSMS_attempt,
			newConsumerId, discount, testDays
		} = this.props.state;

		if (position === 2 && newConsumerId) {
			return  <Redirect to={`/consumer/${newConsumerId}`} />
		}

		return (
			<React.Fragment>
				<p />
				<Card>
					<CardContent>
						<Typography variant="h6">{dictionary.menuJoinConsumer[language]}</Typography>
						{/*-------------------------------------------------- 1 -------------------------------------*/}
						{position >= 0 ? (
							<div style={{marginTop: 20}}>
								<TextField
									fullWidth autoFocus
									variant="outlined"
									id="contact"
									label={dictionary.contact[language]}
									placeholder={dictionary.contactPlaceholder[language]}
									value={contact || ''}
									onChange={this.onChangeInput.bind(this)}
									onKeyPress={this.onKeyPress.bind(this)}
									margin="normal"
									disabled={busy || position !== 0}
								/>
							</div>
						) : null}

						{/*-------------------------------------------------- 2 --------------------------------------*/}
						{position >= 1 ? (
							<div style={{marginTop: 20}}>
								{packets.sort((a, b) => b.groupId - a.groupId).map((packet, key) => <div key={key}>
									<FormControlLabel control={
														  <Switch checked={addedPackets.includes(packet)}
																  onChange={this.onTogglePackets.bind(this, packet)}
																  value={packet.id}
																  inputProps={{'aria-label': ''}}
																  color="primary"
														  />
													  }
													  label={<span>{packet.title} <i style={{color: 'gray'}}>{packet.description}</i></span>}
									/>
								</div>)}
								{/*<br />*/}
								{/*<FormControlLabel control={*/}
								{/*	<Switch id="enableDiscount" checked={enableDiscount}*/}
								{/*			onChange={this.onChangeInput.bind(this)}*/}
								{/*			value={0}*/}
								{/*			inputProps={{'aria-label': ''}}*/}
								{/*	/>*/}
								{/*}*/}
								{/*				  label={dictionary.enableDiscount[language]}*/}
								{/*/>*/}
								<br />
								<FormControlLabel control={
									<Switch id="enableTest" checked={enableTest}
											onChange={this.onChangeInput.bind(this)}
											value={0}
											inputProps={{'aria-label': ''}}
									/>
								}
												  label={
												  	dictionary.enableTest[language]
														? dictionary.enableTest[language].replace('%day%', testDays)
														: 'enable test'}
								/>
								<br/>
								<br/>
								<TextField
									fullWidth autoFocus
									variant="outlined"
									id="code"
									label={dictionary.codePlaceholder[language]}
									placeholder={dictionary.codePlaceholder[language]}
									value={code || ''}
									onChange={this.onChangeInput.bind(this)}
									onKeyPress={this.onKeyPress.bind(this)}
									margin="normal"
									disabled={busy || position !== 1}
								/>
							</div>
						) : null}

						{/*-------------------------------------------------- 3 --------------------------------------*/}

					</CardContent>
					<CardActions>
						{position === 0 ? (
							<Button onClick={this.sendSMS.bind(this)}
									disabled={busy || `${contact}`.length < 5}
									variant="outlined" >
								{dictionary.sendSMS[language]}
							</Button>
						) : position === 1 ? (
							<React.Fragment>
								<Button onClick={this.checkSMS.bind(this)}
										disabled={busy}
										variant="outlined" >
									{dictionary.checkSMS[language]}
								</Button>
								{' '}
								<Button onClick={this.reset.bind(this)}
										disabled={busy}
										variant="outlined" >
									{dictionary.resetBtn[language]}
								</Button>
								{' '}
								<Button onClick={this.sendSMS.bind(this)}
										disabled={busy || disableSendButton}
										variant="outlined" >
									{remainSMS > 0 ? dictionary.sendSMS_again[language] : dictionary.attemptsEnded[language]}
									{' '}
									{timerCounter ? `(${timerCounter})` : null}
								</Button>
							</React.Fragment>
						) : position === 2 ? (
							<Button onClick={this.openConsumer.bind(this)}
									disabled={busy}
									variant="outlined" >
								{dictionary.openConsumer[language]}
							</Button>
						) : null}
					</CardActions>
				</Card>
			</React.Fragment>
		);
	}
}

function mapStateToProps(state) {
	let {
		error, busy, newConsumerId, packets, language, dictionary, referral,
		consumerJoinPosition, remainSMS, nextSMS_attempt, discount, testDays
	} = state;
	return {
		state: {
			error,
			busy,
			newConsumerId,
			packets,
			language,
			dictionary,
			referral,
			consumerJoinPosition, remainSMS, nextSMS_attempt,
			discount, testDays
		}
	}
}

function matDispatchToProps(dispatch) {
	return bindActionCreators({
		dismissAlert, sendSMS2consumer, checkConsumerSMS
	}, dispatch);
}

export default connect(mapStateToProps, matDispatchToProps)(JoinConsumer);
