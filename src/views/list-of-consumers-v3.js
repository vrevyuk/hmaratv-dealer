import React, {createRef} from "react";
import {withRouter} from "react-router";
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import MaterialTable from "material-table";
import axios from "axios";
import {API_ROOT} from "../api";
import moment from "moment";

const dateFormat = 'DD-MM-YYYY HH:mm';

const renderDate = date => {
    return date ? <div style={{whiteSpace: 'nowrap'}}>{moment(date).format(dateFormat)}</div> : ''
};

const renderLastActivity = activity => {
    if (!activity) return '';
    const [date, ip] = activity.split(',');
    return <div>
        <div>{renderDate(date)}</div>
        <div style={{fontSize: 'small'}}>IP: {ip}</div>
    </div>
};

const renderStatus = status => {
    return status === 1
        ? <span style={{color: 'limegreen'}}>online</span>
        : <span style={{color: 'silver'}}>offline</span>
};

const renderTest = row => {
    return row.hasCredits === 0 && moment().isBefore(row.packetExpired)
        ? <span style={{color: 'orangered'}}>ТЕСТ</span>
        : ''
};

const ListOfConsumersV3 = (props) => {

    let {token, language, dictionary} = props;
    axios.defaults.params = {token, language};

    const tableRef = createRef();

    const columns = [
        {title: dictionary.name[language], field: 'name', render: row => <Link to={`/consumer/${row.id}`} >{row.name||row.contact||'user'}</Link>, editable: 'newer'},
        {title: dictionary.comment[language], field: 'description', editable: 'onUpdate'},
        {title: dictionary.balanceTableHeader[language], field: 'balance', render: row => parseFloat(row.balance).toFixed(2), editable: 'newer'},
        {title: dictionary.contact[language], field: 'contact', editable: 'newer'},
        {title: dictionary.packets[language], field: 'packet', editable: 'newer'},
        {title: dictionary.expiredAt[language], field: 'packetExpired', render: row => renderDate(row.packetExpired), editable: 'newer'},
        {title: dictionary.isTest[language], field: 'isTest', render: renderTest, editable: 'newer'},
        {title: dictionary.status[language], field: 'status', render: row => renderStatus(row.online_status), editable: 'newer'},
        {title: dictionary.lastActivity[language], field: 'lastActivity', render: row => renderLastActivity(row.lastActivity), editable: 'newer'},
        {title: dictionary.createdAt[language], field: 'createdAt', render: row => renderDate(row.created), editable: 'newer'},
    ];

    const getData = async (query) => {
        try {
            let {orderBy, orderDirection, page, pageSize, search} = query;
            let url = `${API_ROOT}/dealer/consumers_v3?search=${search}&page=${page}&pageSize=${pageSize}`;
            url += orderBy ? `&orderBy=${orderBy.field}&orderDirection=${orderDirection}` : '';
            let {data} = await axios.get(url);

            return {
                data: data.list,
                page,
                totalCount: data.totalCount,
            }
        } catch (e) {
            window.alert(e.response ? e.response.data : e.message);
            return {data: [], page: 0, totalCount: 0,}
        }
    };

    const onRowUpdate = (newData, oldData) => {
        return axios.post(`${API_ROOT}/dealer/description`, {
            consumerId: newData.id, description: newData.description
        });
    };

    return <MaterialTable title={dictionary.menuListConsumers[language]}
                          tableRef={tableRef}
                          columns={columns}
                          data={getData}
                          onChangePage={page => page}
                          actions={[
                              {
                                  icon: 'refresh',
                                  tooltip: 'Refresh Data',
                                  isFreeAction: true,
                                  onClick: () => tableRef.current && tableRef.current.onQueryChange(),
                              }
                          ]}
                          options={{
                              pageSize: 10,
                              pageSizeOptions: [5, 10, 20, 100, 500, 1000],
                              debounceInterval: 1000,
                              exportButton: true
                          }}
                          editable={{
                              isEditable: () => true,
                              onRowUpdate: onRowUpdate
                          }}/>
};

function mapStateToProps(state) {
    let {token, language, dictionary} = state;
    return {token, language, dictionary}
}

export default withRouter(connect(mapStateToProps)(ListOfConsumersV3));