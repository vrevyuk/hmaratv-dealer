/**
 * Created by Vitaly Revyuk on 10/31/18.
 */
import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Nav, Navbar, NavItem } from 'react-bootstrap'

import { logout } from '../api'

class MainMenu extends React.Component {

	onSelect(e) {
		switch (e) {
			case 'logout':
				this.props.logout(() => this.props.history.push(`/`));
				break;
			default:
				this.props.history.push(`/${e}`);
				break;
		}
	}

	render() {
		let { name, language, dictionary = {} } = this.props;
		let { menuJoinConsumer, menuListConsumers, menuMyPayments, menuSummary, menuLogout, playListGenerator } = dictionary;

		return <Navbar onSelect={e=>this.onSelect(e)} collapseOnSelect={true} fluid inverse>
			<Navbar.Header>
				<Navbar.Brand>
					<span>{name}</span>
				</Navbar.Brand>
				<Navbar.Toggle />
			</Navbar.Header>
			<Navbar.Collapse>
				<Nav>
					<NavItem eventKey={'join'}>{menuJoinConsumer[language]}</NavItem>
					<NavItem eventKey={'list'}>{menuListConsumers[language]}</NavItem>
					<NavItem eventKey={'credits'}>{menuMyPayments[language]}</NavItem>
					<NavItem eventKey={'plGenerator'}>{playListGenerator[language]}</NavItem>
					<NavItem eventKey={'summary'}>{menuSummary[language]}</NavItem>
				</Nav>
				<Nav pullRight>
					<NavItem eventKey={'logout'}>{menuLogout[language]}</NavItem>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	}
}

function mapStateToProps(state) {
	let { name, language, dictionary } = state;
	return {
		name,
		language,
		dictionary,
	}
}

function matDispatchToProps(dispatch) {
	return {
		logout: (cb) => logout(dispatch, cb)
	}
}

export default connect(mapStateToProps, matDispatchToProps)(withRouter(MainMenu));