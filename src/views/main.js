import React from 'react'
import {connect} from 'react-redux'
import {Route, withRouter} from 'react-router'
import {bindActionCreators} from "redux";

import ListConsumers from './list-consumers'
import ListOfConsumersV2 from './list-of-consumers-v2'
import ListOfConsumersV3 from './list-of-consumers-v3'
import JoinConsumer from './join-consumer'
import Credits from './credits'
import Summary from './summary'
import Consumer from './consumer'
import ToggleLanguage from './toggle-language'

import {makeStyles} from '@material-ui/core/styles';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";

import MenuIcon from "@material-ui/icons/Menu"
import ConnectIcon from '@material-ui/icons/CastConnected';
import CustomersIcon from '@material-ui/icons/People';
import PaymentsIcon from '@material-ui/icons/Payment';
import ListIcon from '@material-ui/icons/List';
import SummaryIcon from '@material-ui/icons/Assessment';
import LogoutIcon from '@material-ui/icons/ExitToApp';
import Divider from "@material-ui/core/Divider";

import logout from '../actions/logout'
import {Typography} from "@material-ui/core";

const classes = makeStyles(theme => ({
	root: {
		flexGrow: 1,
	},
	menuButton: {
		marginRight: theme.spacing(2),
	},
	title: {
		flexGrow: 1,
	},
}));

class MainView extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			openDrawer: false,
		};
	}

	onClick(action) {
		setTimeout(() => {
			switch (action) {
				case 'logout':
					return this.setState({openDrawer: false}, () => {
						this.props.logout();
					});
				case 'toggleDrawer':
					return this.setState({openDrawer: !this.state.openDrawer});
				default:
					return this.setState({openDrawer: false}, () => {
						this.props.history.push(`${action}`);
					});
			}
		}, 250);
	}

	render() {

		let {openDrawer}  =this.state;
		let {language, dictionary = {}, version, id} = this.props.state;
		let {appTitle, menuJoinConsumer, menuListConsumers, menuMyPayments, menuSummary, menuLogout} = dictionary;

		const toolBarColor = /hmara.tv/.test(window.location.hostname) ? 'inherit' : 'green';

		return (
			<React.Fragment>
				<AppBar position="static">
					<Toolbar style={{backgroundColor: toolBarColor}}>
						<IconButton edge="start"
									className={classes.menuButton}
									color="inherit"
									aria-label="menu"
									onClick={this.onClick.bind(this, 'toggleDrawer')}>
							<MenuIcon/>
						</IconButton>
						<div style={{flexGrow: 1}}><Typography>{window.location.hostname.toUpperCase()}</Typography></div>
						<ToggleLanguage/>
					</Toolbar>
				</AppBar>
				<Drawer open={openDrawer}
						onClose={this.onClick.bind(this, 'toggleDrawer')}>
					<List>
						<ListItem selected={this.props.location.pathname === '/'}>
							<ListItemText primary={appTitle[language] + ' ver ' + version}/>
						</ListItem>
						<Divider/>
						<ListItem button
								  onClick={this.onClick.bind(this, '/join')}
								  selected={this.props.location.pathname === '/join'}>
							<ListItemIcon><ConnectIcon/></ListItemIcon>
							<ListItemText primary={menuJoinConsumer[language]}/>
						</ListItem>
						<ListItem button
								  onClick={this.onClick.bind(this, '/customers')}
								  selected={this.props.location.pathname === '/customers'}>
							<ListItemIcon><CustomersIcon/></ListItemIcon>
							<ListItemText primary={menuListConsumers[language]}/>
						</ListItem>
						<ListItem button
								  onClick={this.onClick.bind(this, '/credits')}
								  selected={this.props.location.pathname === '/credits'}>
							<ListItemIcon><PaymentsIcon/></ListItemIcon>
							<ListItemText primary={menuMyPayments[language]}/>
						</ListItem>
						<ListItem button
								  onClick={this.onClick.bind(this, '/summary')}
								  selected={this.props.location.pathname === '/summary'}>
							<ListItemIcon><SummaryIcon/></ListItemIcon>
							<ListItemText primary={menuSummary[language]}/>
						</ListItem>
						<Divider/>
						<ListItem button onClick={this.onClick.bind(this, 'logout')}>
							<ListItemIcon><LogoutIcon/></ListItemIcon>
							<ListItemText primary={menuLogout[language]}/>
						</ListItem>
					</List>
				</Drawer>
				<Route path="/" exact component={Summary}/>
				<Route path="/join" component={JoinConsumer}/>
				<Route path="/customers" component={[43452, 14].includes(id) ? ListOfConsumersV3 : ListConsumers}/>
				<Route path="/credits" component={Credits}/>
				<Route path="/consumer/:id" component={Consumer}/>
				<Route path="/summary" component={Summary}/>
			</React.Fragment>
		)
	}
}

function mapStateToProps(state) {
	let {id, dealerId, busy, name, language, dictionary, version} = state;
	return {
		state: {
			id, dealerId, busy, name, language, dictionary, version
		}
	}
}

function matDispatchToProps(dispatch) {
	return bindActionCreators({
		logout
	}, dispatch);
}

export default withRouter(connect(mapStateToProps, matDispatchToProps)(MainView));