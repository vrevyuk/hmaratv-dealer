/**
 * Created by Vitaly Revyuk on 10/31/18.
 */
import React from 'react'
import { connect } from 'react-redux'
import { Panel, Form, FormGroup, InputGroup, FormControl, Glyphicon, Button, Alert, ListGroup, ListGroupItem } from 'react-bootstrap'
import { searchConsumers } from '../api'


class ReferralLinks extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		}
	}

	render() {

		let { error, busy, refLinks = [], dictionary, language } = this.props;

		return (
			<Panel>
				<Panel.Heading>
					{dictionary.menuMyReferralLinks[language]}
				</Panel.Heading>
				<Panel.Body><ListGroup>{refLinks.map( (link, key) =>
					<ListGroupItem key={key}><a href={link.referralBaseURL.replace('[refCode]', link.refCode)} target="_new">{link.description}</a></ListGroupItem>
				)}</ListGroup></Panel.Body>
			</Panel>
		)
	}
}

function mapStateToProps(state, ownProps) {
	let { busy, dealerId, refLinks, dictionary, language, error } = state;
	console.log(state);
	return {
		busy,
		dealerId,
		refLinks,
		dictionary,
		language,
		error,
	}
}

function mapDispatchToProps(dispatch, ownProps) {
	return {
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ReferralLinks);