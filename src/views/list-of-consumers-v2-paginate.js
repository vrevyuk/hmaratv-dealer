import React, {useEffect, useState} from "react";
import TablePagination from "@material-ui/core/TablePagination";

const ConsumersPaginate = (props) => {

    let {total, onChange} = props;

    let [page, setPage] = useState(0);
    let [rowsPerPage, setRowsPerPage] = useState(10);

    useEffect(() => {
        typeof onChange === "function" && onChange(page, rowsPerPage, 1);
    }, [page]);

    useEffect(() => {
        page === 0 ? (typeof onChange === "function" && onChange(page, rowsPerPage, 2)) : setPage(0);
    }, [rowsPerPage]);

    return <TablePagination
        rowsPerPageOptions={[5, 10, 25, 50, 100]}
        component="div"
        count={total}
        rowsPerPage={rowsPerPage}
        page={total === 0 ? 0 : page}
        backIconButtonProps={{
            'aria-label': 'previous page',
        }}
        nextIconButtonProps={{
            'aria-label': 'next page',
        }}
        onChangePage={(e, p) => setPage(p)}
        onChangeRowsPerPage={e => setRowsPerPage(parseInt(e.target.value, 10))}
    />
};

export default ConsumersPaginate;