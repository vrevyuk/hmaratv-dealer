import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {withRouter} from 'react-router'

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';
import pl from 'moment/locale/pl';

import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import {Toolbar} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import TablePagination from "@material-ui/core/TablePagination";

import searchConsumers from '../actions/get-all-sonsumers'
import pagination from "../actions/pagination";
import consumersSorting from "../actions/consumers-sorting";

class ListConsumers extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			en, uk, ru, pl,
			pageIndex: 0,
			rowsPerPage: 10,
			search: '',
			// orderField: 'id',
			// order: {
			// 	id: {direction: 'desc'},
			// 	name: {direction: 'desc'},
			// 	balance: {direction: 'desc'},
			// 	currency: {direction: 'desc'},
			// 	created: {direction: 'desc'},
			// }
		}
	}

	componentDidMount() {
		this.searchConsumer();
	}

	searchConsumer() {
		this.props.searchConsumers();
	}

	showConsumer(consumerId) {
		window.open(`/#/consumer/${consumerId}`, "_blank");
		// this.props.history.push(`/consumer/${consumerId}`);
	}

	sortHandler(column) {
		let {
			order = {},
		} = this.props.state;

		this.props.consumersSorting({
			orderField: column,
			direction: order[column].direction === 'asc' ? 'desc' : 'asc'
		});
	}

	textFieldHandler(e) {
		let {value} = e.currentTarget;
		this.setState({pageIndex: 0, search: value});
	}

	handleChangePage(e, pageIndex) {
		this.props.pagination({
			section: 'consumers',
			pageIndex
		});
	}

	handleChangeRowsPerPage(e) {
		this.props.pagination({
			section: 'consumers',
			pageIndex: 0,
			rowsPerPage: e.target.value
		});
	}

	render() {

		let {search = ''} = this.state;
		let {
			consumers = [], language, dictionary = {},
			consumersPagination: {
				pageIndex = 0, rowsPerPage = 10
			} = {},
			orderField, order = {},
		} = this.props.state;
		moment.locale(language);

		consumers = consumers.sort((a, b) => {
			switch (orderField) {
				case 'name':
					return order[orderField].direction === 'desc' ? `${a.name}`.localeCompare(`${b.name}`) : `${b.name}`.localeCompare(`${a.name}`);
				case 'balance':
					return order[orderField].direction === 'desc' ? a.balance - b.balance : b.balance - a.balance;
				case 'currency':
					return order[orderField].direction === 'desc' ? `${a.currency}`.localeCompare(`${b.currency}`) : `${b.currency}`.localeCompare(`${a.currency}`);
				case 'phone':
					return order[orderField].direction === 'desc' ? `${a.phone}`.localeCompare(`${b.phone}`) : `${b.phone}`.localeCompare(`${a.phone}`);
				case 'created':
					return order[orderField].direction === 'desc' ? moment(a.created).unix() - moment(b.created).unix() : moment(b.created).unix() - moment(a.created).unix();
				default:
					return 0;
			}
		}).filter( item => {
			if (search.length > 0) {
				let {name, phone, description} = item;
				return new RegExp(`${search}`, 'i').test(`${name}`)
					|| new RegExp(`${search}`, 'i').test(`${phone}`)
					|| new RegExp(`${search}`, 'i').test(`${description}`);
			}
			return true;
		});

		return (
			<React.Fragment>
				<Toolbar>
					<TextField
						autoFocus={true}
						fullWidth
						id="search"
						label={dictionary.search[language]}
						placeholder={dictionary.searchPlaceholder[language]}
						value={search}
						onChange={this.textFieldHandler.bind(this)}
						margin="normal"
					/>
				</Toolbar>
				<br />
				<Table size="small">
					<TableHead>
						<TableRow>
							<TableCell>
								#
							</TableCell>
							<TableCell>
								<TableSortLabel
									active={orderField === 'name'}
									direction={order['name'] ? order['name'].direction : 'desc'}
									onClick={this.sortHandler.bind(this, 'name')}
								>name</TableSortLabel>
							</TableCell>
							<TableCell>
								<TableSortLabel
									active={orderField === 'balance'}
									direction={order['balance'] ? order['balance'].direction : 'desc'}
									onClick={this.sortHandler.bind(this, 'balance')}
								>balance</TableSortLabel>
							</TableCell>
							<TableCell>
								<TableSortLabel
									active={orderField === 'currency'}
									direction={order['currency'] ? order['currency'].direction : 'desc'}
									onClick={this.sortHandler.bind(this, 'currency')}
								>currency</TableSortLabel>
							</TableCell>
							<TableCell>
								<TableSortLabel
									active={orderField === 'phone'}
									direction={order['phone'] ? order['phone'].direction : 'desc'}
									onClick={this.sortHandler.bind(this, 'phone')}
								>phone</TableSortLabel>
							</TableCell>
							<TableCell>
								<TableSortLabel
									active={orderField === 'created'}
									direction={order['created'] ? order['created'].direction : 'desc'}
									onClick={this.sortHandler.bind(this, 'created')}
								>created</TableSortLabel>
							</TableCell>
							<TableCell>description</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{consumers
							.slice(pageIndex * rowsPerPage, (pageIndex + 1) * rowsPerPage)
							.map((row, key) => <TableRow hover
														 role="checkbox"
														 tabIndex={-1}
														 onClick={this.showConsumer.bind(this, row.id)}
														 key={key}>
							<TableCell>{key + 1}</TableCell>
							<TableCell>{row.name}</TableCell>
							<TableCell>{row.balance}</TableCell>
							<TableCell>{row.currency}</TableCell>
							<TableCell>{row.phone}</TableCell>
							<TableCell>{moment(row.created).format('DD MMMM YYYY HH:mm')}</TableCell>
							<TableCell>{row.description}</TableCell>
						</TableRow>)}
					</TableBody>
				</Table>
				<TablePagination
					rowsPerPageOptions={[5, 10, 25, 50, consumers.length]}
					component="div"
					count={consumers.length}
					rowsPerPage={rowsPerPage}
					page={consumers.length === 0 ? 0 : pageIndex}
					backIconButtonProps={{
						'aria-label': 'previous page',
					}}
					nextIconButtonProps={{
						'aria-label': 'next page',
					}}
					onChangePage={this.handleChangePage.bind(this)}
					onChangeRowsPerPage={this.handleChangeRowsPerPage.bind(this)}
				/>
			</React.Fragment>
		)
	}
}

function mapStateToProps(state) {
	let {
		busy, dealerId, consumers, token, language, dictionary, error, consumersPagination, orderField, order
	} = state;
	return {
		state: {
			busy, dealerId, token, language, dictionary, consumers, error, consumersPagination, orderField, order
		}
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		searchConsumers, pagination, consumersSorting
	}, dispatch)
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListConsumers));