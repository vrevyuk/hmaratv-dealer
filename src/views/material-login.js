/**
 * Created by Vitalii Reviuk today.
 */
import React, {Fragment, useState} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'

import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";

import requestCode from '../actions/request-code';
import checkOutCode from '../actions/check-code';
import resetAuth from '../actions/reset-auth';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";

const MLogin = (props) => {

	let [contact, setContact] = useState('');
	let [code, setCode] = useState('');
	let [rememberMe, setRememberMe] = useState(false);
	let [captcha, setCaptcha] = useState("");

	let {
		busy,
		sentSMS,
		captchaRequired,
		checkedSMS,
		language,
		dictionary,
	} = props.state;

	function onClick(e) {
		let { id } = e.currentTarget;
		switch (id) {
			case 'request-code':
				if (contact < 5) return;
				props.requestCode({contact, captcha});
				break;
			case 'check-out-code':
				props.checkOutCode({contact, code, rememberMe});
				break;
			case 'reset':
				setCode('');
				setContact('');
				props.resetAuth();
				break;
			default: break;
		}
	}

	return (
		<Container maxWidth='sm'>
			<form>
				<Typography variant="h4">{dictionary.authentication[language]}</Typography>
				<TextField
					autoFocus={!sentSMS}
					fullWidth
					id="contact"
					label={dictionary.contact[language]}
					placeholder={dictionary.contactPlaceholder[language]}
					value={contact}
					onChange={e => setContact(e.currentTarget.value)}
					margin="normal"
					disabled={busy || sentSMS}
					variant="outlined"
				/>
				{captchaRequired && <Fragment>
					<div style={{display: "flex", alignItems: "center", justifyContent: "center", gap: "10px"}}>
						<img src={captchaRequired.captchaImage}/>
						<TextField
							id="captcha"
							fullWidth
							label={dictionary.captchaPlaceholder[language]}
							value={captcha}
							onChange={e => setCaptcha(e.currentTarget.value)}
							margin="normal"
							variant="outlined"
						/>
					</div>
				</Fragment>}
				<br />
				<TextField
					autoFocus={sentSMS}
					fullWidth
					id="code"
					label={dictionary.codePlaceholder[language]}
					value={code}
					onChange={e => setCode(e.currentTarget.value)}
					margin="normal"
					disabled={busy || !sentSMS}
					variant="outlined"
				/>
				<br />
				<br />
				<FormControlLabel
					control={
						<Switch checked={rememberMe}
								onChange={e => setRememberMe(e.currentTarget.checked)}
								value="remember-me"
								color="primary"
								disabled={busy || (sentSMS && checkedSMS)}/>
					}
					label={dictionary.saveSessionLabel[language]}
				/>
				<br />
				<br />
				{
					!sentSMS ? <Button disabled={busy}
									   variant="contained"
									   color="primary"
									   id="request-code"
									   type="submit"
									   onClick={onClick}>
							{dictionary.sendSMS[language]}
						</Button>
						: sentSMS && !checkedSMS ? <Button disabled={busy}
														   variant="contained"
														   color="primary"
														   type="submit"
														   id="check-out-code"
														   onClick={onClick}>
							{dictionary.checkSMS[language]}
						</Button>
						: null
				}
				{' '}
				<Button disabled={busy}
						variant="contained"
						color="secondary"
						id="reset"
						onClick={onClick}>
					Reset
				</Button>
			</form>
		</Container>
	)
};

function mapStateToProps(state) {
	let {refreshToken, error, busy, sentSMS, captchaRequired, checkedSMS, id, name, language, dictionary} = state;
	return {
		state: {refreshToken, error, busy, sentSMS, captchaRequired, checkedSMS, id, name, language, dictionary}
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		requestCode, checkOutCode, resetAuth
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MLogin);