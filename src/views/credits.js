import React from 'react'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux'

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';
import pl from 'moment/locale/pl';

import getConfig from '../actions/get-config'
import pagination from "../actions/pagination";

import {TableHead} from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";

import CreditIcon from "@material-ui/icons/CallReceived";
import DebitIcon from "@material-ui/icons/CallMade";
import TablePagination from "@material-ui/core/TablePagination";

class Credits extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, pl, ru,
		}
	}

	componentDidMount() {
		this.props.getConfig();
	}

	handleChangePage(e, pageIndex) {
		this.props.pagination({
			section: 'credits',
			pageIndex
		});
		// this.setState({pageIndex});
	}

	handleChangeRowsPerPage(e) {
		this.props.pagination({
			section: 'credits',
			pageIndex: 0,
			rowsPerPage: e.target.value
		});
		// this.setState({pageIndex: 0, rowsPerPage: e.target.value});
	}

	render() {

		let { credits = [], language, creditsPagination: { pageIndex = 0, rowsPerPage = 10 } = {} } = this.props.state;
		moment.locale(language);

		// credits = credits.sort( (a, b) => moment(b['timestamp']).unix() - moment(a['timestamp']).unix() );

		return (
			<React.Fragment>
				<Table size="small">
					<TableHead>
						<TableRow>
							<TableCell>#</TableCell>
							<TableCell>sum</TableCell>
							<TableCell>timestamp</TableCell>
							<TableCell>description</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{
							credits
								.slice(pageIndex * rowsPerPage, (pageIndex + 1) * rowsPerPage)
								.map((credit, key) => <TableRow key={key} style={{color: 'red'}}>
								<TableCell>{key + 1} {credit['payType'] === 'credit' ? <CreditIcon color="primary"/> :
									<DebitIcon color="secondary"/>}</TableCell>
								<TableCell>{(credit['sum'] || 0).toFixed(2)}</TableCell>
								<TableCell>{moment(credit['timestamp']).format('DD MMMM YYYY HH:mm')}</TableCell>
								<TableCell>{credit['description']}</TableCell>
							</TableRow>)
						}
					</TableBody>
				</Table>
				<TablePagination
					rowsPerPageOptions={[5, 10, 25, 50, credits.length]}
					component="div"
					count={credits.length}
					rowsPerPage={rowsPerPage}
					page={credits.length === 0 ? 0 : pageIndex}
					backIconButtonProps={{
						'aria-label': 'previous page',
					}}
					nextIconButtonProps={{
						'aria-label': 'next page',
					}}
					onChangePage={this.handleChangePage.bind(this)}
					onChangeRowsPerPage={this.handleChangeRowsPerPage.bind(this)}
				/>
			</React.Fragment>
		)
	}
}

function mapStateToProps(state) {
	let {
		error, time, busy, dealerId, name, balance, profit, credits,
		consumers, messages, packets, token, language, creditsPagination
	} = state;
	return {
		state: {
			error, time, busy, dealerId, name, balance, profit, credits,
			consumers, messages, packets, token, language, creditsPagination
		}
	}
}

function matDispatchToProps(dispatch) {
	return bindActionCreators({
		getConfig, pagination
	}, dispatch);
}

export default connect(mapStateToProps, matDispatchToProps)(Credits);