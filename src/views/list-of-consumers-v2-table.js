import React, {useState} from "react";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import {TableCell, TableRow} from "@material-ui/core";

const ConsumersTable = (props) => {

    return <Table>
        <TableHead>
            <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Balance</TableCell>
                <TableCell>Phone</TableCell>
                <TableCell>Created</TableCell>
                <TableCell>Tariff</TableCell>
                <TableCell>Test</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Last IP</TableCell>
                <TableCell>Last logIn</TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
        </TableBody>
    </Table>
};

export default ConsumersTable;