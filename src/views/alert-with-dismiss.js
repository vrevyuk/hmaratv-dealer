/**
 * Created by Vitaly Revyuk on 11/1/18.
 */
import React from 'react'
import { connect } from 'react-redux'
import { Alert } from 'react-bootstrap'
import { dismissAlert } from '../api'

class AlertWithDismiss extends React.Component {

	componentDidMount() {
		this.timer = setTimeout(() => this.props.dismissAlert(), 4000)
	}

	componentWillUnmount() {
		clearTimeout(this.timer);
	}

	render() {

		let { error } = this.props;

		return (
			<Alert bsStyle="danger" onClick={()=>this.props.dismissAlert()}>
				{error}
			</Alert>
		)
	}
}

function mapStateToProps(state) {
	let { error } = state;
	return {
		error,
	}
}

function matDispatchToProps(dispatch) {
	return {
		dismissAlert: () => dismissAlert(dispatch),
	}
}

export default connect(mapStateToProps, matDispatchToProps)(AlertWithDismiss);