import React, {useState} from "react";
import {bindActionCreators} from "redux";
import searchConsumers from "../actions/get-all-sonsumers";
import pagination from "../actions/pagination";
import consumersSorting from "../actions/consumers-sorting";
import {withRouter} from "react-router";
import {connect} from "react-redux";

import TextField from "@material-ui/core/TextField";
import {Toolbar} from "@material-ui/core";
import ConsumersTable from "./list-of-consumers-v2-table";
import ConsumersPaginate from "./list-of-consumers-v2-paginate";

const ListOfConsumersV2 = (props) => {

    let {dictionary, language, consumers} = props;

    let [search, setSearch] = useState('');

    const changePage = (page, rowsPerPage, place) => {
        console.log({page, rowsPerPage, place});
    };

    return <React.Fragment>
        <Toolbar>
            <TextField
                autoFocus={true}
                fullWidth
                id="search"
                label={dictionary.search[language]}
                placeholder={dictionary.searchPlaceholder[language]}
                value={search}
                onChange={e => setSearch(e.target.value)}
                margin="normal"
            />
        </Toolbar>
        <ConsumersTable />
        <ConsumersPaginate total={consumers.length || 200} onChange={changePage} />
    </React.Fragment>
};

function mapStateToProps(state) {
    let {
        busy, dealerId, consumers, token, language, dictionary, error, consumersPagination, orderField, order
    } = state;
    return {busy, dealerId, token, language, dictionary, consumers, error, consumersPagination, orderField, order}
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        searchConsumers, pagination, consumersSorting
    }, dispatch)
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListOfConsumersV2));