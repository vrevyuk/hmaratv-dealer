import React from 'react'
import {connect} from "react-redux"
import {bindActionCreators} from "redux"

import IconButton from '@material-ui/core/IconButton'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

import uk_flag from '../assets/ukraine.png'
import us_flag from '../assets/united-states.png'
import ru_flag from '../assets/russia.png'
import pl_flag from '../assets/poland.png'

import changeLanguage from '../actions/change-language'
import Image from "react-bootstrap/lib/Image"

class ToggleLanguage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            languages: ['en', 'uk', 'ru', 'pl']
        }
    }

    handleClick(lang, e) {
        let {changeLanguage} = this.props;

        switch (lang) {
            case 'show':
                this.setState({anchorEl: e.currentTarget});
                break;
            case 'hide':
                this.setState({anchorEl: null});
                break;
            case 'uk':
            case 'en':
            case 'ru':
            case 'pl':
                this.setState({anchorEl: null}, () => changeLanguage(lang));
                break;
            default:
                break;
        }
    }

    render() {

        let {languages = [], anchorEl} = this.state;
        let {language} = this.props.state;

        return <div>
            <IconButton aria-controls="simple-menu"
                        aria-haspopup="true"
                        onClick={this.handleClick.bind(this, 'show')}>
                {language === 'uk' && <Image alt="uk" width={20} src={uk_flag}/>}
                {language === 'en' && <Image alt="en" width={20} src={us_flag}/>}
                {language === 'ru' && <Image alt="ru" width={20} src={ru_flag}/>}
                {language === 'pl' && <Image alt="ru" width={20} src={pl_flag}/>}
            </IconButton>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={this.handleClick.bind(this, 'hide')}>
                {languages.map(
                    (lang, key) =>
                        <MenuItem key={key}
                                  selected={language === lang}
                                  onClick={this.handleClick.bind(this, lang)}>
                            {lang.toUpperCase()}
                        </MenuItem>
                )}

            </Menu>
        </div>;
    }
}

function mapStateToProps(state) {
    let {busy, name, language, dictionary} = state;
    return {
        state: {
            busy, name, language, dictionary
        }
    }
}

function matDispatchToProps(dispatch) {
    return bindActionCreators({
        changeLanguage
    }, dispatch);
}

export default connect(mapStateToProps, matDispatchToProps)(ToggleLanguage);