import React from 'react'
import { connect } from 'react-redux'
import {bindActionCreators} from "redux";

import moment from 'moment';
import uk from 'moment/locale/uk';
import en from 'moment/locale/en-gb';
import ru from 'moment/locale/ru';
import pl from 'moment/locale/pl';

import getConsumer from '../actions/get-consumer'
import transferMoney from '../actions/transfer-money'
import unJoin from '../actions/un-join'
import saveDescription from '../actions/save-consumer-description'
import resetJoiningConsumer from '../actions/reset-joining-consumer'

import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Toolbar from "@material-ui/core/Toolbar";
import Input from "@material-ui/core/Input";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";

import UserIcon from "@material-ui/icons/Person";
import PhoneIcon from "@material-ui/icons/Phone";
import BalanceIcon from "@material-ui/icons/AttachMoney";
import SaveIcon from "@material-ui/icons/Save";
import SendIcon from "@material-ui/icons/Send";
import LiveTv from "@material-ui/icons/LiveTv";
import List from "@material-ui/core/List";
import {ListItemIcon} from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Grid from "@material-ui/core/Grid";

class Consumer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			uk, en, ru, pl,
			descriptionLocal: null
		}
	}

	componentDidMount() {
		let { id: consumerId } = this.props.match.params;
		this.props.getConsumer({consumerId});
	}

	transferSum() {
		let {id: consumerId} = this.props.match.params;
		let {transferSum} = this.state;
		this.props.transferMoney({consumerId, sum: parseFloat(transferSum) || 0});
	}

	unjoin() {
		let { token, language } = this.props.state;
		let { id: consumerId } = this.props.match.params;
		if (window.confirm('Do you want to disconnect this consumer?')) {
			this.props.unJoin({ token, language, consumerId, cb: () => this.props.history.goBack() });
		}
	}

	saveDescription() {
		let {id: consumerId} = this.props.match.params;
		let {descriptionLocal} = this.state;
		this.props.saveDescription({consumerId, description: descriptionLocal});
	}

	goBack() {
		this.props.history.goBack();
	}

	repeatAgainClick() {
		this.props.resetJoiningConsumer();
		this.props.history.push('/join');
	}

	onChange(e) {
		let { id, value } = e.target;
		this.setState({[id]: value});
	}

	render() {
		let {busy, consumer = {}, language, dictionary = {}, consumerJoinPosition, newConsumerId} = this.props.state;
		let {name = '', balance, currency = 'NZ', phone, playlist, credits = [], description} = consumer;
		let {descriptionLocal, transferSum} = this.state;
		balance = parseFloat(balance) || 0;
		console.info({consumerJoinPosition, newConsumerId});
		return (
			<React.Fragment>
				<List>
					<ListItem>
						<ListItemIcon><UserIcon /></ListItemIcon>
						<ListItemText>Имя пользователя</ListItemText>
						<ListItemSecondaryAction><Typography>{name}</Typography></ListItemSecondaryAction>
					</ListItem>
					<ListItem>
						<ListItemIcon><UserIcon /></ListItemIcon>
						<ListItemText>Контакт</ListItemText>
						<ListItemSecondaryAction><Typography>{phone}</Typography></ListItemSecondaryAction>
					</ListItem>
					<ListItem>
						<ListItemIcon><UserIcon /></ListItemIcon>
						<ListItemText>Ссьілка плейлиста</ListItemText>
						<ListItemSecondaryAction><Typography>{playlist}</Typography></ListItemSecondaryAction>
					</ListItem>
					<ListItem>
						<ListItemIcon><UserIcon /></ListItemIcon>
						<ListItemText>Баланс, грн</ListItemText>
						<ListItemSecondaryAction><Typography>{balance.toFixed(2)}</Typography></ListItemSecondaryAction>
					</ListItem>
				</List>

				<Card style={{marginTop: 20, backgroundColor: '#EEEEEE'}}>
					<CardContent>
						<Grid container>
							<Grid item xl={9} lg={9} md={9} sm={12} xs={12}>
								<TextField
									id="descriptionLocal"
									placeholder="Описание"
									fullWidth
									multiline
									rows="8"
									variant="outlined"
									onChange={this.onChange.bind(this)}
									value={descriptionLocal || description || ''}
									disabled={busy}
								/>
							</Grid>
							<Grid item xl={1} lg={1} md={1} sm={12} xs={12}>&nbsp;</Grid>
							<Grid container alignItems="flex-end" justify="flex-end">
								<Button variant="outlined" onClick={this.saveDescription.bind(this)}
										disabled={busy || (descriptionLocal === null) || (descriptionLocal === description)}>
									сохранить
								</Button>
							</Grid>
						</Grid>
					</CardContent>
				</Card>

				{consumerJoinPosition === 2 && newConsumerId > 0 ? (
					<Card>
						<CardContent>
							<Button onClick={this.repeatAgainClick.bind(this)}
									disabled={busy}
									variant="outlined" >
								{dictionary.repeatAgain[language]}
							</Button>
						</CardContent>
					</Card>
				) : (
					<React.Fragment>
						<Card>
							<CardContent>
								<Grid container>
									<Grid item xl={9} lg={9} md={9} sm={12} xs={12}>
										<Input
											id="transferSum"
											placeholder="сумма для перевода"
											fullWidth
											onChange={this.onChange.bind(this)}
											value={transferSum}
											disabled={busy}
										/>
									</Grid>
									<Grid item xl={1} lg={1} md={1} sm={12} xs={12}>&nbsp;</Grid>
									<Grid container xl={2} lg={2} md={2} sm={12} xs={12} alignItems="flex-end" justify="flex-end">
										<Button variant="outlined"
												onClick={this.transferSum.bind(this)}
												disabled={busy || !(parseFloat(transferSum) > 0)}>
											Перевести
										</Button>
									</Grid>
								</Grid>
							</CardContent>
						</Card>

						<Card>
							<CardContent>
								<Table size="small">
									<TableHead>
										<TableRow>
											<TableCell>Сумма</TableCell>
											<TableCell>Дата транзакции</TableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{
											credits.map((credit, key) => <TableRow key={key} style={{color: 'red'}}>
												<TableCell>{
													(credit['sum'] || 0).toFixed(2)}
													{' '}
													{credit['sum'] ? credit['currency'].toLowerCase() : null}
												</TableCell>
												<TableCell>{moment(credit['timestamp']).format('DD-MM-YY HH:mm')}</TableCell>
											</TableRow>)
										}
									</TableBody>
								</Table>
							</CardContent>
						</Card>
					</React.Fragment>
				)}
			</React.Fragment>
		)
	}
}

function mapStateToProps(state) {
	let {error, busy, token, consumer, language, dictionary,consumerJoinPosition, newConsumerId} = state;
	return {
		state: {
			error, busy, token, consumer, language, dictionary, consumerJoinPosition, newConsumerId
		}
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		getConsumer, transferMoney, unJoin, saveDescription, resetJoiningConsumer
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Consumer);