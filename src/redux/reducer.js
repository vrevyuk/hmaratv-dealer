/**
 * Created by Vitaly Revyuk on 10/31/18.
 */

const defaultState = {
	version: process.env.REACT_APP_VERSION,
	error: false,
	time: new Date().toLocaleString(),
	busy: false,
	sentSMS: false,
	captchaRequired: false,
	checkedSMS: false,
	language: localStorage.getItem('language') || 'ru',
	dealerId: -1,
	id: -2,
	name: '',
	balance: 0,
	profit: 0,
	discount: 0,
	testDays: 0,
	credits: [],
	messages: [],
	consumers: [],
	consumer: {},
	packets: {},
	token: '',
	referralUrl: '',
	refreshToken: localStorage.getItem('refreshToken'),
	consumerJoinPosition: 0,
	remainSMS: 0,
	nextSMS_attempt: 0,
	newConsumerId: 0,
	creditsPagination: {
		pageIndex: 0,
		rowsPerPage: 10
	},
	consumersPagination: {
		pageIndex: 0,
		rowsPerPage: 10
	},
	orderField: 'created',
	order: {
		name: {direction: 'desc'},
		balance: {direction: 'desc'},
		currency: {direction: 'desc'},
		phone: {direction: 'desc'},
		created: {direction: 'asc'},
	},

	dictionary: {
		appTitle: { en: 'Dealer page', uk: 'Дилерська сторінка', ru: 'Дилерская страница', pl: 'Polska - Dealer' },
		authentication: { en: 'Authentication', uk: 'Автентифікація', ru: 'Аутентификация', pl: 'Authentication' },
		contact: { en: 'contact', uk: 'контакт', ru: 'контакт', pl: 'contact' },
		contactPlaceholder: { en: 'phone number in format 380XXXXXXXXX without plus ', uk: 'номер телефону в форматі 380XXXXXXXXX без плюса', ru: 'номер телефона в формате 380XXXXXXXXX без плюса', pl: 'Ogólnoświatowy numer telefonu' },
		captchaPlaceholder: { en: 'Enter numbers from image ', uk: 'Введіть цифри з зображення ', ru: 'Enter numbers from image ', pl: 'Enter numbers from image ' },
		codePlaceholder: { en: 'code in SMS', uk: 'код з СМС', ru: 'код из СМС', pl: 'kod z SMS'  },
		restoreSessionBtn: { en: 'Restore session', uk: 'Відновити сесію', ru: 'Восстановить сессию', pl: 'Przywróć sesję'  },
		removeSessionBtn: { en: 'Remove session', uk: 'Видалити сесію', ru: 'Удалить сессию', pl: 'Usuń sesję'  },
		saveSessionLabel: { en: 'save session', uk: 'зберегти сесію', ru: 'сохранить сессию', pl: 'zapisz sesję'  },
		menuJoinConsumer: { en: 'Join new consumer', uk: 'Приєднати користувача', ru: 'Присоединить пользователя', pl: 'Dołącz użytkownika'  },
		menuListConsumers: { en: 'List of my consumers', uk: 'Мої користувачі', ru: 'Мои пользователи', pl: 'Moi użytkownicy'  },
		menuMyPayments: { en: 'My payments', uk: 'Мої платежі', ru: 'Мои платежи', pl: 'Moje płatności'  },
		menuSummary: { en: 'Summary', uk: 'Загальне', ru: 'Общее', pl: 'Ogólne'  },
		menuDoPayment: { en: 'Pay', uk: 'Платіж', ru: 'Платеж', pl: 'Płatność'  },
		menuLogout: { en: 'Log out', uk: 'Вихід', ru: 'Выход', pl: 'Wyjdź'  },
		menuMyReferralLinks: { en: 'Referral links', uk: 'Реферальні посилання', ru: 'Реферальные ссылки', pl: 'Linki polecające'  },
		selectTariff: { en: 'Select tariff one or more if needed', uk: 'Виберіть тариф, один чи більше', ru: 'Выберите тариф', pl: 'Wybierz stawkę'  },
		resetBtn: { en: 'Reset', uk: 'Скинути', ru: 'Сбросить', pl: 'Zresetuj'  },
		name: { en: 'Name', uk: 'Ім\'я', ru: 'Имя', pl: 'Imię'  },
		balance: { en: 'Balance', uk: 'Баланс', ru: 'Баланс', pl: 'Saldo'  },
		profit: { en: 'Profit, %', uk: 'Винагорода, %', ru: 'Доход, %', pl: 'Dochód, %'  },
		discount: { en: 'User discount, %', uk: 'Знижка для користувача, %', ru: 'Скидка для пользователя, %', pl: 'User discount, %'  },
		referralUrl: { en: 'Referral link', uk: 'Реферальне посилання', ru: 'Реферальная ссылка', pl: 'Link referencyjny'  },
		// languageLabel: { en: 'Language', uk: 'Мова', ru: 'Язык', pl: 'Język'  },
		findButton: { en: 'Find', uk: 'Знайти', ru: 'Найти', pl: 'Znajdź'  },
		phoneTableHeader: { en: 'phone', uk: 'телефон', ru: 'телефон', pl: 'telefon'  },
		nameTableHeader: { en: 'name', uk: 'ім\'я', ru: 'имя', pl: 'imię'  },
		balanceTableHeader: { en: 'balance', uk: 'баланс', ru: 'баланс', pl: 'saldo'  },
		transferBtn: { en: 'Transfer', uk: 'Передати', ru: 'Перевести', pl: 'Transfer'  },
		backBtn: { en: 'Back', uk: 'Назад', ru: 'Назад', pl: 'Powrót'  },
		search: { en: 'Search', uk: 'Пошук', ru: 'Поиск', pl: 'Szukaj'  },
		searchPlaceholder: { en: 'id, phone, name, description', uk: 'id, phone, name, description', ru: 'id, phone, name, description', pl: 'id, phone, name, description'  },
		enableDiscount: { en: 'enable discount', uk: 'підключити знижку', ru: 'дать скидку', pl: 'enable discount'  },
		enableTest: { en: 'enable test period for %day% days.', uk: 'включити тестовий період на %day% день(дні) ', ru: 'дать тестовый период на %day% день(дней) ', pl: 'enable test period for %day% days'  },
		sendSMS: { en: 'send code', uk: 'відправити код', ru: 'отправить код', pl: 'send code'  },
		sendSMS_again: { en: 'send code again', uk: 'відправити код ще раз', ru: 'отправить код еще раз', pl: 'send code'  },
		attemptsEnded: { en: 'attempts ended', uk: 'спроби закінчились', ru: 'попытки закончились', pl: 'attempts ended'  },
		checkSMS: {en: 'check sms', uk: 'перевірити код', ru: 'проверить код', pl: 'enable discount'},
		openConsumer: {en: 'open consumer', uk: 'перейти на абонента', ru: 'перейти на абонента', pl: 'open consumer'},
		playListGenerator: {en: 'Playlist generator', uk: 'Генератор плейлитстів', ru: 'Генератор плейлистов', pl: 'Generator listy odtwarzania'},
		comment: {en: 'comment', uk: 'коментар', ru: 'комментарий', pl: 'komentarz'},
		createdAt: {en: 'created at', uk: 'створено', ru: 'создано', pl: 'utworzony w'},
		lastActivity: {en: 'last activity', uk: 'остання активність', ru: 'последняя активность', pl: 'Ostatnia aktywność'},
		status: {en: 'status', uk: 'статус', ru: 'статус', pl: 'status'},
		isTest: {en: 'test', uk: 'тест', ru: 'тест', pl: 'próba'},
		expiredAt: {en: 'expired at', uk: 'закінчується', ru: 'окончание', pl: 'wygasł o'},
		packets: {en: 'packets', uk: 'пакети', ru: 'пакеты', pl: 'pakiety'},
		repeatAgain: {en: 'repeat again', uk: 'повторити ще раз', ru: 'повторить еще раз', pl: 'repeat again'},
		xxx: {en: '', uk: '', ru: '', pl: ''},
	}
};

function reducer(state = defaultState, action) {
	let { type, payload } = action;

	switch (type) {
		case 'LANGUAGE': return { ...state, language: payload };
		case 'I_AM_BUSY': return {...state, busy: true, error: false};
		case 'I_AM_NOT_BUSY': return {...state, busy: false};
		case 'RESET_AUTH': return {...state, busy: false, sentSMS: false, captchaRequired: false, checkedSMS: false};
		case 'CAPTCHA_REQUIRED': return {
			...state,
			busy: false,
			sentSMS: false,
			captchaRequired: payload,
			checkedSMS: false
		};
		case 'SMS_IS_SENT': return {...state, busy: false, sentSMS: true, captchaRequired: false, checkedSMS: false};
		case 'ERROR': return { ...state, busy: false, consumerJoinPosition: 0, error: payload };
		case 'I_AM_LOGGED': return { ...state,
			busy: false,
			captchaRequired: false,
			checkedSMS: true,
			id: payload.id,
			dealerId: payload.dealerId,
			language: payload.language,
			name: payload.name,
			balance: payload.balance,
			profit: payload.profit,
			discount: payload.discount,
			testDays: payload.testDays,
			credits: payload.credits || [],
			consumers: payload.consumers || [],
			messages: payload.messages || [],
			packets: payload.packets || [],
			referralUrl: payload.referralUrl || [],
			token: payload.token,
			consumer: {},
			error: false,
			consumerJoinPosition: 0,
			remainSMS: 0,
			nextSMS_attempt: 0,
		};
		case 'I_AM_NOT_LOGGED': return { ...state,
			busy: false,
			checkedSMS: false,
			sentSMS: false,
			language: localStorage.getItem('language') || 'uk',
			dealerId: 0,
			id: 0,
			name: '',
			balance: 0,
			profit: 0,
			discount: 0,
			credits: [],
			consumers: [],
			messages: [],
			packets: [],
			referralUrl: '',
			token: '',
			refreshToken: '',
			consumer: {},
			error: false,
			consumerJoinPosition: 0,
			remainSMS: 0,
			nextSMS_attempt: 0,
		};
		case 'CONSUMERS': return {...state, busy: false, consumers: payload, consumer: {}, error: false};
		case 'CONSUMER': return {...state, busy: false, consumer: payload, error: false};
		case 'SET_JOIN_POSITION': {
			const {consumerJoinPosition, remainSMS, nextSMS_attempt} = payload;
			return {...state, busy: false, consumerJoinPosition, remainSMS, nextSMS_attempt, error: false};
		}
		case 'SET_JOIN_POSITION_FINAL': {
			let {consumerJoinPosition, newConsumerId} = payload;
			return {...state, busy: false, consumerJoinPosition, remainSMS: 0, nextSMS_attempt: 0, newConsumerId, error: false};
		}
		case 'CREDITS_PAGINATION_SET_PAGE': {
			let {pageIndex, rowsPerPage} = payload;
			return {...state, creditsPagination: {pageIndex, rowsPerPage}};
		}
		case 'CONSUMERS_PAGINATION_SET_PAGE': {
			let {pageIndex, rowsPerPage} = payload;
			return {...state, consumersPagination: {pageIndex, rowsPerPage}};
		}
		case 'CONSUMERS_SORTING_SET': {
			let {orderField, order} = payload;
			let {consumersPagination} = state;
			return {...state, order, orderField, consumersPagination: {...consumersPagination, pageIndex: 0}};
		}
		default: return state;
	}
}

export default reducer;