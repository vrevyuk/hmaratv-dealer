import axios from "axios"
import {API_ROOT} from "../api"
import getConsumer from './get-consumer'

export default function ({consumerId, sum}) {
    return async (dispatch, getState) => {
        try {
            if (sum === 0) return;
            dispatch({type: 'I_AM_BUSY'});
            let {token, language} = getState();
            axios.defaults.data = {token, language};
            let {status} = await axios.post(`${API_ROOT}/dealer/transfer/${consumerId}`, {
                sum,
            });

            if (status === 200) {
                getConsumer({consumerId, token})(dispatch, getState);
            }
        } catch (error) {
            dispatch({
                type: 'ERROR',
                payload: error.response ? error.response.data : error.message,
            })
        }
    }
}