import axios from "axios";
import {API_ROOT} from "../api";

export default function ({contact, code, packets, enableDiscount, enableTest}) {
    return async (dispatch, getState) => {
        try {
            if (!code) return window.alert('code mustn\'t be empty');
            dispatch({type: 'I_AM_BUSY'});
            let {token, language} = getState();
            axios.defaults.data = {token, language};

            let {status, data} = await axios.post(`${API_ROOT}/dealer/join`, {
                contact, code, enableDiscount, enableTest, packets, byDealerApp: true
            });

            if (status === 200) {
                dispatch({
                    type: 'SET_JOIN_POSITION_FINAL',
                    payload: {
                        consumerJoinPosition: 2,
                        newConsumerId: data,
                    }
                });
            }
        } catch (error) {
            // window.alert(error.response ? error.response.data : error.message);
            window.alert('Неверньій код');
            dispatch({type: 'I_AM_NOT_BUSY'});
        }
    }
}
