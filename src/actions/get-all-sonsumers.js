import axios from "axios";
import qs from "qs";
import {API_ROOT} from "../api";

export default function () {
    return async (dispatch, getState) => {
        try {
            dispatch({type: 'I_AM_BUSY'});
            let {token, language} = getState();
            axios.defaults.params = {token, language};
            let {status, data} = await axios.get(`${API_ROOT}/dealer/consumers?${qs.stringify({
            })}`);

            console.log(status, data);
            if (status === 200) {
                dispatch({
                    type: 'CONSUMERS',
                    payload: data,
                })
            }
        } catch (error) {
            dispatch({
                type: 'ERROR',
                payload: error.response ? error.response.data : error.message,
            })
        }
    }
}