export default function (language) {
    return (dispatch, getState) => {
        if (language) {
            localStorage.setItem('language', language);
            dispatch({
                type: 'LANGUAGE',
                payload: language
            });
        }
    };
}