import axios from "axios";
import {API_ROOT} from "../api";

export default function ({consumerId, description}) {
    return async (dispatch, getState) => {
        try {
            dispatch({type: 'I_AM_BUSY'});
            let {token, language} = getState();
            axios.defaults.data = {token, language};
            let {status} = await axios.post(`${API_ROOT}/dealer/description`, {
                consumerId, description
            });

            if (status === 200) {
                dispatch({
                    type: 'CONSUMER',
                    payload: {...getState().consumer, description}
                });
            }
        } catch (error) {
            dispatch({
                type: 'ERROR',
                payload: error.response ? error.response.data : error.message,
            })
        }
    }
}