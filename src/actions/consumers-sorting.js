export default function ({orderField, direction}) {
    return (dispatch, getState) => {
        let {
            order
        } = getState();

        console.log({orderField, direction, order});
        dispatch({
            type: 'CONSUMERS_SORTING_SET',
            payload: {
                orderField,
                order: {
                    ...order,
                    [orderField]: {direction}
                },
            },
        });
    }
}