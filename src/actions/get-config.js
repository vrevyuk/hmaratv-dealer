import axios from "axios";
import {API_ROOT} from "../api";

export default function ({accessToken} = {}) {
    return async (dispatch, getState) => {
        try {
            dispatch({type: 'I_AM_BUSY'});
            let {token, language} = getState();
            token = accessToken || token;

            axios.defaults.params = {token, language};
            let {status, data} = await axios.get(`${API_ROOT}/dealer`);

            if (status === 200) {
                let {id, name, phone, balance, profit, credits, consumers, messages, packets, referralUrl, discount, testDays} = data;
                // console.log({id, name, phone, balance, profit, credits, consumers, messages, packets, referralUrl, discount, testDays});
                dispatch({
                    type: 'I_AM_LOGGED',
                    payload: {
                        id,
                        language,
                        name,
                        phone,
                        balance,
                        credits,
                        consumers,
                        messages,
                        packets,
                        profit,
                        discount,
                        testDays,
                        token,
                        referralUrl
                    },
                });
            }
        } catch (error) {
            dispatch({
                type: 'ERROR',
                payload: error.response ? error.response.data : error.message,
            })
        }
    }
}
