export default function ({section, pageIndex, rowsPerPage}) {
    return (dispatch, getState) => {
        let {
            creditsPagination: {
                pageIndex: prevCreditsPageIndex,
                rowsPerPage: prevCreditsRowPerPage,
            },
            consumersPagination: {
                pageIndex: prevConsumersPageIndex,
                rowsPerPage: prevConsumersRowPerPage,
            }} = getState();

        if (section === 'credits') {
            dispatch({
                type: 'CREDITS_PAGINATION_SET_PAGE',
                payload: {
                    pageIndex: isNaN(parseInt(pageIndex, 10)) ? prevCreditsPageIndex : parseInt(pageIndex, 10),
                    rowsPerPage: isNaN(parseInt(rowsPerPage, 10)) ? prevCreditsRowPerPage : parseInt(rowsPerPage, 10)
                },
            });
        } else if (section === 'consumers') {
            dispatch({
                type: 'CONSUMERS_PAGINATION_SET_PAGE',
                payload: {
                    pageIndex: isNaN(parseInt(pageIndex, 10)) ? prevConsumersPageIndex : parseInt(pageIndex, 10),
                    rowsPerPage: isNaN(parseInt(rowsPerPage, 10)) ? prevConsumersRowPerPage : parseInt(rowsPerPage, 10)
                },
            });
        }
    }
}