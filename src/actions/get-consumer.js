import axios from "axios";
import {API_ROOT} from "../api";

export default function ({consumerId}) {
    return async (dispatch, getState) => {
        try {
            dispatch({type: 'I_AM_BUSY'});
            let {token, language} = getState();
            axios.defaults.params = {token, language};
            let {status, data} = await axios.get(`${API_ROOT}/dealer/consumers/${consumerId}`);

            if (status === 200) {
                console.log(data);
                dispatch({
                    type: 'CONSUMER',
                    payload: data,
                })
            }
        } catch (error) {
            dispatch({
                type: 'ERROR',
                payload: error.response ? error.response.data : error.message,
            })
        }
    }
}