export default function () {
    return (dispatch, getState) => {
        localStorage.removeItem('refreshToken');
        dispatch({
            type: 'I_AM_NOT_LOGGED',
        });

    }
}