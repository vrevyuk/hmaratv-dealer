import axios from "axios";
import qs from "qs";
import {API_ROOT} from "../api";

export default function ({contact, captcha}) {
    return async (dispatch, getState) => {
        dispatch({ type: 'I_AM_BUSY' });
        try {
            const data = {contact};
            const {captchaRequired} = getState();
            if (captchaRequired) {
                data["captchaId"] = captchaRequired.captchaId;
                data["captchaText"] = captcha;
            }
            let {status} = await axios.get(
              `${API_ROOT}/sign?${qs.stringify(data)}`
            );

            if (status === 200) {
                dispatch({type: 'SMS_IS_SENT'});
            }
        } catch (error) {
            if (error.response && error.response.status === 412) {
                const {captchaId, captchaImage, expiredAt} = error.response.data;
                return dispatch({
                    type: 'CAPTCHA_REQUIRED',
                    payload: { captchaId, captchaImage, expiredAt }
                });
            }
            dispatch({
                type: 'ERROR',
                payload: error.response ? error.response.data : error.message,
            })
        }
    }
}
