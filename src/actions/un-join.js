import axios from "axios";
import {API_ROOT} from "../api";

export default function ({consumerId}) {
    return async (dispatch, getState) => {
        try {
            dispatch({type: 'I_AM_BUSY'});
            let {token, language} = getState();
            axios.defaults.data = {token, language};
            let {status, data} = await axios.delete(`${API_ROOT}/dealer/join/${consumerId}`);
            if (status === 200) {
                console.log('unJoined', consumerId, data);
            }
        } catch (error) {
            dispatch({
                type: 'ERROR',
                payload: error.response ? error.response.data : error.message,
            })
        }
    }
}