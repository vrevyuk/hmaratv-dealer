import axios from 'axios';
import qs from 'qs';
import {API_ROOT} from '../api';
import getConfig from './get-config'

export default function ({contact, code, refreshToken, rememberMe}) {
    return async (dispatch, getState) => {
        dispatch({type: 'I_AM_BUSY'});

        try {
            let {data, status} = await axios.get(`${API_ROOT}/sign?${qs.stringify({
                refreshToken,
                contact,
                code
            })}`);

            if (status === 200) {
                let {refreshToken, accessToken} = data;
                if (refreshToken && rememberMe) localStorage.setItem('refreshToken', refreshToken);
                getConfig({accessToken})(dispatch, getState);
            }
        } catch (e) {
            if (e.response.status === 401) {
                return dispatch({
                    type: 'I_AM_NOT_LOGGED',
                });
            }

            dispatch({
                type: 'ERROR',
                payload: e.response ? e.response.data : e.message,
            })
        }
    }
}
