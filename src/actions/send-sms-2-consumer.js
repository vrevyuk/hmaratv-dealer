import axios from "axios";
import {API_ROOT} from "../api";

export default function ({contact, cb}) {
    return async (dispatch, getState) => {
        try {
            dispatch({type: 'I_AM_BUSY'});
            let {token, language} = getState();
            axios.defaults.data = {token, language};
            let {status, data} = await axios.post(`${API_ROOT}/dealer/join`, {
                contact
            });

            // console.log({status, data});
            if (status === 200) {
                dispatch({
                    type: 'SET_JOIN_POSITION',
                    payload: {
                        consumerJoinPosition: 1,
                        remainSMS: data['remain'],
                        nextSMS_attempt: data['nextAt']
                    }
                });

                typeof cb === "function" && cb();
            }

        } catch (error) {
            alert(error.response ? error.response.data : error.message);
            dispatch({
                type: 'ERROR',
                payload: error.response ? error.response.data : error.message,
            })
        }
    }
}