/**
 * Created by Vitaly Revyuk on 10/31/18.
 */

const API_ROOT = process.env.REACT_APP_API_URL;

export {API_ROOT}