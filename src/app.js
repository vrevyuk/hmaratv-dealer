/**
 * Created by Vitaly Revyuk on 10/31/18.
 */
import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {HashRouter} from 'react-router-dom'

import Main from './views/main'
import Login from "./views/material-login";

import checkCode from './actions/check-code'
import {Typography} from "@material-ui/core";

class App extends React.Component {

	componentDidMount() {
		let {
			refreshToken,
			checkedSMS,
			language,
		} = this.props.state;

		if (!checkedSMS && refreshToken) {
			this.props.checkCode({language, refreshToken});
		}
	}

	render() {
		let {
			refreshToken,
			token,
			checkedSMS,
		} = this.props.state;

		return <HashRouter>
			{token ? <Main/>
				: !checkedSMS && refreshToken
					? <Typography>I'm trying to restore previous session ...</Typography>
					: <Login/>}
		</HashRouter>;
	}

}


function mapStateToProps(state, ownProps) {
	let {refreshToken, token, error, busy, sentSMS, checkedSMS, id, name, language, dictionary} = state;

	return {
		state: {
			refreshToken, token, error, busy, sentSMS, checkedSMS, id, name, language, dictionary
		}
	}
}

function mapDispatchToProps(dispatch, ownProps) {
	return bindActionCreators({
		checkCode
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);